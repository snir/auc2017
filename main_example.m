%% Init

close all;
clear;
clc;

NUM_OF_EXPERIMNETS = 20;
AUC_CALCULATIONS = 200;

addpath('algorithms')

%% Run single algorithm

% Uncomment the following line, set dataset name and replace <PATH> with a valid mat file representing a dataset
% datasetStructInfo = DatasetStructInfo(<DATASET_NAME>, <PATH>);

% For OAM_SEQ and OAM_GRA, uncomment the following line and set <C> parameter.
% parametersSet = containers.Map('C', <C>);

% For OPAUC and ADAOAM, uncomment the following line and set <gamma_regularization> and <eta> parameters.
% parametersSet = containers.Map({'gamma_regularization', 'eta'}, {<gamma_regularization>, <eta>});

% Uncomment one of the following lines
% algorithmObj = OAM_SEQ(parametersSet);
% algorithmObj = OAM_GRA(parametersSet);
% algorithmObj = OPAUC(parametersSet);
% algorithmObj = ADAOAM(parametersSet);
% algorithmObj = KNNOAM();

aucLoss = zeros(NUM_OF_EXPERIMNETS, AUC_CALCULATIONS);

for k=1:NUM_OF_EXPERIMNETS
    % Shuffle samples from dataset
    order = randperm(datasetStructInfo.n);

	X = datasetStructInfo.X(:, order);
	Y = datasetStructInfo.Y(order);

	aucCalculations = zeros(1, datasetStructInfo.n);
	aucCalculations(round(linspace(1, datasetStructInfo.n, AUC_CALCULATIONS))) = 1;

	experimentAucLoss = algorithmObj.runOnlineSetting(X, Y, aucCalculations);
    aucLoss(k, :) = experimentAucLoss(aucCalculations == 1);
end

%% Plot mean emprical AUC loss
figure;

indices = round(linspace(1, datasetStructInfo.n, AUC_CALCULATIONS));
plot(indices, mean(aucLoss))

title(datasetStructInfo.name)
xlabel('samples')
ylabel('empirical AUC loss')
