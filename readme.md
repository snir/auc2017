How to run a single experiment?

Step 1: Create a dataset mat file
A dataset mat file must contain two matrices:
1. X, a d by n matrix representing the samples (d is the dimension and n is the number of samples).
2. Y, a 1 by n matrix representing the labels (the labels must be -1 and 1).

You may use a simple parsing function named [libsvm2mat] that can be found under the [tools] directory. This function parses datasets in LIBSVM format (see: https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/).
You can also generate this mat file on your own. 

Step 2: Edit main_example file
1. Uncomment the datasetStructInfo line and set the dataset's name and path to the mat file from step 1. 
2. Uncomment one of the parametersSet lines and set needed parameters for the algorithm you want to test.
3. Uncomment the algorithm you want to test.
4. Press F5 to run the file.

The plotted graph shows the mean of the AUC loss for NUM_OF_EXPERIMENTS runs. 