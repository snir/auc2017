classdef OAM < AucLinearClassificationModel
    properties
        N_PLUS
        N_MINUS
    end
    
    methods
        function obj = OAM(parametersSet)
            obj@AucLinearClassificationModel(parametersSet)

            obj.N_PLUS = 100;
            obj.N_MINUS = 100;
        end
        
        function [w, aucLoss] = run(obj, X, Y, aucCalculations)
            C = obj.parametersSet('C');
            
            [d, T] = size(X);
            aucLoss = zeros(1, T);
            
            w = zeros(d, 1);
            
            Nt_plus = 0;
            Nt_minus = 0;
            
            Bt_plus = [];
            Bt_minus = [];
            
            for t=1:T
				if aucCalculations(t) == 1 
					aucLoss(t) = obj.sufferLinearLoss(X, Y, w, t);
				end
                
                xt = X(:, t);
                yt = Y(t);
                if yt == 1
                    Nt_plus = Nt_plus + 1;
                    Ct = C * max(1, Nt_minus / obj.N_MINUS);
                    Bt_plus = obj.updateBuffer(Bt_plus, xt, obj.N_PLUS, Nt_plus);
                    w = obj.updateClassifier(w, xt, yt, Ct, Bt_minus);
                elseif yt == -1
                    Nt_minus = Nt_minus + 1;
                    Ct = C * max(1, Nt_plus / obj.N_PLUS);
                    Bt_minus = obj.updateBuffer(Bt_minus, xt, obj.N_MINUS, Nt_minus);
                    w = obj.updateClassifier(w, xt, yt, Ct, Bt_plus);
                else
                    throw('Training set labels must be either +1 or -1!');
                end
            end
        end
        
        function Bt = updateBuffer(~, Bt, xt, N, Nt)
            n = size(Bt, 2);
            if n < N
                Bt = [Bt xt];
            else
                if rand < N / Nt
                    index = randsample(n, 1);
                    Bt(:, index) = xt;
                end
            end
        end
    end

    methods(Abstract, Static)
        w = updateClassifier(w, xt, yt, Ct, Bt);
    end
end