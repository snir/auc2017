classdef AucLinearClassificationModel < OnlineAucMaximizationAlgorithm
    properties
        parametersSet
    end
    
    methods
        function obj = AucLinearClassificationModel(parametersSet)
            obj.parametersSet = parametersSet;
        end
        
        function loss = sufferLinearLoss(~, X, Y, w, t)
            if t == 1
                loss = 0;
                return
            end
            
            T_plus = sum(Y(1:t) == +1);
            T_minus = sum(Y(1:t) == -1);
            if T_plus * T_minus == 0
                loss = 0;
                return
            end
            
            Xt_1 = X(:, 1:t-1);
            Yt_1 = Y(1:t-1);

            R = w' * Xt_1;
                
            [I, J] = meshgrid(R(Yt_1 == 1), R(Yt_1 == -1));
            auc = sum(I(:) > J(:)) + 0.5 * sum(I(:) == J(:));

            loss = 1 - auc / (T_plus * T_minus);
        end
        
        function w = learnClassifier(obj, X, Y)
            [w, ~] = obj.run(X, Y, zeros(size(Y)));
        end
        
        function auc = testClassifier(~, classifier, testSet, testSetLabels)
            X_plus = testSet(:, testSetLabels == 1);
            X_minus = testSet(:, testSetLabels == -1);
            
            T_plus = length(find(testSetLabels == 1));
            T_minus = length(find(testSetLabels == -1));
            
            [I, J] = meshgrid(classifier' * X_plus, classifier' * X_minus);
            s = sum(I(:) > J(:)) + 0.5 * sum(I(:) == J(:));
            
            auc = s / (T_plus * T_minus);
        end
        
    end

    methods(Abstract)
        [w, aucLoss] = run(obj, X, Y, aucCalculations)
    end
end