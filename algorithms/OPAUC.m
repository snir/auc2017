classdef OPAUC < AucLinearClassificationModel
    methods
        function obj = OPAUC(parametersSet)
            obj@AucLinearClassificationModel(parametersSet)
        end
        
        function [w, aucLoss] = run(obj, X, Y, aucCalculations)
            gammaRegularization = obj.parametersSet('gamma_regularization');
            eta = obj.parametersSet('eta');
            
            % d is the dimension of the samples
            % T is the number of the samples
            [d, T] = size(X);
            aucLoss = zeros(1, T);
            
            w = zeros(d, 1);
            
            T_plus = 0;
            T_minus = 0;
            
            ct_plus = zeros(d, 1);
            ct_minus = zeros(d, 1);

            Gamma_plus = zeros(d);
            Gamma_minus = zeros(d);
            
            St_plus_helper = zeros(d);
            St_minus_helper = zeros(d);
            
            for t=1:T
				if aucCalculations(t) == 1 
					aucLoss(t) = obj.sufferLinearLoss(X, Y, w, t);
				end
                
                xt = X(:, t);
                yt = Y(t);
                if yt == 1
                    T_plus = T_plus + 1;
                    St_plus_helper = St_plus_helper + xt * xt';
                    
                    ct_plus_new = ct_plus + (xt - ct_plus) / T_plus;
                    Gamma_plus = obj.updateGamma(Gamma_plus, xt, T_plus, ct_plus, ct_plus_new);
                    gradient = obj.getGradient(w, gammaRegularization, xt, yt, ct_minus, St_minus_helper, T_minus);
                    
                    ct_plus = ct_plus_new;
                else
                    T_minus = T_minus + 1;
                    St_minus_helper = St_minus_helper + xt * xt';
                    
                    ct_minus_new = ct_minus + (xt - ct_minus) / T_minus;
                    Gamma_minus = obj.updateGamma(Gamma_minus, xt, T_minus, ct_minus, ct_minus_new);
                    gradient = obj.getGradient(w, gammaRegularization, xt, yt, ct_plus, St_plus_helper, T_plus);
                    
                    ct_minus = ct_minus_new;
                end
                
                w = w - eta * gradient;
            end
        end
        
        function Gamma = updateGamma(~, Gamma, xt, T, ct_old, ct_new)
            Gamma = Gamma + ((xt * xt') - Gamma) / T + ct_old * ct_old' - ct_new * ct_new';
        end
        
        function gradient = getGradient(~, w, gammaRegularization, xt, yt, ct_new, St_helper, T)
            if T == 0
                gradient = zeros(size(w));
            else
                St = St_helper / T - ct_new * ct_new';
                gradient = gammaRegularization * w - yt * xt + yt * ct_new + (xt - ct_new) * (xt - ct_new)' * w + St * w;
            end
        end
    end
end