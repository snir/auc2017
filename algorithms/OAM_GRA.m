classdef OAM_GRA < OAM
    methods
        function obj = OAM_GRA(parametersSet)
            obj@OAM(parametersSet)
        end
        
        function w = updateClassifier(~, wt, xt, yt, Ct, Bt)
            n = size(Bt, 2);
            w = wt;
            for i=1:n
                x = Bt(:, i);
                if yt * (wt' * (xt - x)) <= 1
                    w = w + Ct * yt * (xt - x) / 2;
                end
            end
        end
    end
end