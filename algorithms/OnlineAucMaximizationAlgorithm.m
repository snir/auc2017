classdef OnlineAucMaximizationAlgorithm < handle
    methods
        function aucLoss = runOnlineSetting(obj, X, Y, aucCalculations)
            [~, aucLoss] = obj.run(X, Y, aucCalculations);
        end
    end
    
    methods(Abstract)
        [classifier, aucLoss] = run(obj, X, Y, aucCalculations)
        classifier = learnClassifier(obj, trainingSet, trainingSetLabels);
        auc = testClassifier(obj, classifier, testSet, testSetLabels);
    end
end