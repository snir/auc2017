classdef OAM_SEQ < OAM
    methods
        function obj = OAM_SEQ(parametersSet)
            obj@OAM(parametersSet)
        end
        
        function w = updateClassifier(~, wt, xt, yt, Ct, Bt)
            n = size(Bt, 2);
            
            for i=1:n
                x = Bt(:, i);
                tau = min(Ct / 2, hingeLoss(wt, yt * (xt - x)) / norm(xt - x, 2));
                wt = wt + tau * yt * (xt - x);
            end
            
            w = wt;
        end
    end
end

function loss = hingeLoss(w, t)
    loss = max(0, 1 - w' * t);
end