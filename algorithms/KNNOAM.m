classdef KNNOAM < OnlineAucMaximizationAlgorithm
    methods
        function [classifier, aucLoss] = run(obj, X, Y, aucCalculations)
            classifier = struct('X', X, 'Y', Y);
            
            % d is the dimension of the samples
            % T is the number of the samples
            [~, T] = size(X);
            aucLoss = zeros(1, T);
            
            for t=1:T
				if aucCalculations(t) == 1
					aucLoss(t) = obj.sufferKNNLoss(X, Y, t);
				end
            end
        end
        
        function classifier = learnClassifier(obj, X, Y)
            [classifier, ~] = obj.run(X, Y, zeros(size(Y)));
        end
        
        function auc = testClassifier(obj, classifier, testSet, testSetLabels)
            k = obj.getK(size(classifier.X, 2));
            [IDX] = knnsearch(classifier.X', testSet', 'k', k);
            etaValues = sum(classifier.Y(IDX')) / k;
                        
            T_plus = length(find(testSetLabels == 1));
            T_minus = length(find(testSetLabels == -1));
            
            [I, J] = meshgrid(etaValues(testSetLabels == 1), etaValues(testSetLabels == -1));
            s = sum(I(:) > J(:)) + 0.5 * sum(I(:) == J(:));
            
            auc = s / (T_plus * T_minus);
        end
        
        function loss = sufferKNNLoss(obj, X, Y, t)
            if t == 1
                loss = 0;
                return
            end
            
            T_plus = sum(Y(1:t) == +1);
            T_minus = sum(Y(1:t) == -1);
            if T_plus * T_minus == 0
                loss = 0;
                return
            end
            
            k = obj.getK(t);

            Xt_1 = X(:, 1:t-1);
            Yt_1 = Y(1:t-1);

            [IDX] = knnsearch(Xt_1', Xt_1', 'k', k + 1);
            R = (sum(Y(IDX')) - Yt_1) / k;

            [I, J] = meshgrid(R(Yt_1 == 1), R(Yt_1 == -1));
            auc = sum(I(:) > J(:)) + 0.5 * sum(I(:) == J(:));

            loss = 1 - auc / (T_plus * T_minus);
        end
    
        function k = getK(~, t)
            k = max(1, 2 * floor(log(t) / log(2)));
        end
            
    end
end