classdef ADAOAM < AucLinearClassificationModel
    properties
        delta
    end
    methods
        function obj = ADAOAM(parametersSet)
            obj@AucLinearClassificationModel(parametersSet)
            
            obj.delta = 1e-15;
        end
        
        function [w, aucLoss] = run(obj, X, Y, aucCalculations)
            gammaRegularization = obj.parametersSet('gamma_regularization');
            eta = obj.parametersSet('eta');
            
            % d is the dimension of the samples
            % T is the number of the samples
            [d, T] = size(X);
            aucLoss = zeros(1, T);
            
            w = zeros(d, 1);
            
            T_plus = 0;
            T_minus = 0;
            
            ct_plus = zeros(d, 1);
            ct_minus = zeros(d, 1);

            Gamma_plus = zeros(d);
            Gamma_minus = zeros(d);
            
            St_plus_helper = zeros(d);
            St_minus_helper = zeros(d);
            
            s_squared_plus = zeros(d, 1);
            s_squared_minus = zeros(d, 1);
            
            for t=1:T
				if aucCalculations(t) == 1 
					aucLoss(t) = obj.sufferLinearLoss(X, Y, w, t);
				end
                
                xt = X(:, t);
                yt = Y(t);
                if yt == 1
                    T_plus = T_plus + 1;
                    St_plus_helper = St_plus_helper + xt * xt';
                    
                    ct_plus_new = ct_plus + (xt - ct_plus) / T_plus;
                    Gamma_plus = obj.updateGamma(Gamma_plus, xt, T_plus, ct_plus, ct_plus_new);
                    
                    gt = obj.getGradient(w, gammaRegularization, xt, yt, ct_minus, St_minus_helper, T_minus);
                    
                    s_squared_plus = s_squared_plus + gt .^ 2;
                    
                    adjustedGradient = gt ./ (obj.delta + sqrt(s_squared_minus));
                    
                    ct_plus = ct_plus_new;
                elseif yt == -1
                    T_minus = T_minus + 1;
                    St_minus_helper = St_minus_helper + xt * xt';
                    
                    ct_minus_new = ct_minus + (xt - ct_minus) / T_minus;
                    Gamma_minus = obj.updateGamma(Gamma_minus, xt, T_minus, ct_minus, ct_minus_new);
                    gt = obj.getGradient(w, gammaRegularization, xt, yt, ct_plus, St_plus_helper, T_plus);
                    
                    s_squared_minus = s_squared_minus + gt .^ 2;
                    
                    adjustedGradient = gt ./ (obj.delta + sqrt(s_squared_plus));
                    
                    ct_minus = ct_minus_new;
                else
                    throw('Training set labels must be either +1 or -1!');
                end
                
                w = obj.piFunction(1 / sqrt(gammaRegularization), w - eta * adjustedGradient);
            end
        end
        
        function Gamma = updateGamma(~, Gamma, xt, T, ct_old, ct_new)
            Gamma = Gamma + ct_old * ct_old' - ct_new * ct_new' + ((xt * xt') - Gamma - ct_old * ct_old') / T;
        end
        
        function gradient = getGradient(~, w, gammaRegularization, xt, yt, ct_new, St_helper, T)
            if T == 0
                gradient = zeros(size(w));
            else
                St = St_helper / T - ct_new * ct_new';
                gradient = gammaRegularization * w - yt * xt + yt * ct_new + (xt - ct_new) * (xt - ct_new)' * w + St * w;
            end
        end
        
        function new_v = piFunction(~, x, v)
            new_v = min(1, x / norm(v)) * v;
        end
    end
end