classdef KNNClassifier < handle
    properties
        X
        Y
        k
    end
    
    methods
        function obj = KNNClassifier(X, Y)
            obj.X = X;
            obj.Y = Y;
            obj.k = 2 * floor(log(size(X, 2)) / log(2));
        end
    end
end