function libsvm2mat(filename, num_of_samples, num_of_features, output_filename)
    X = zeros(num_of_features, num_of_samples);
    Y = zeros(1, num_of_samples);
    
    fid = fopen(filename);

    count = 1;
    tline = fgetl(fid);
    while ischar(tline)
        C = strsplit(strtrim(tline));
        Y(count) = str2double(C(1));
        
        A = cell2mat(cellfun(@(x) cellfun(@str2double, strsplit(x, ':')'), C(2:end), 'UniformOutput', false));
        if ~isempty(A)
            X(sub2ind(size(X), A(1, :), count * ones(size(A(1, :))))) = A(2, :);
        end

        tline = fgetl(fid);
        count = count + 1;
    end

    fclose(fid);
    
    save(fullfile(output_filename), 'X', 'Y');
end
