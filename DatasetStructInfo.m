classdef DatasetStructInfo 
    properties
        name
        datasetMat
        X
        Y
        ratio
        d
        n
    end
    methods  
        function obj = DatasetStructInfo(nDataset, datasetMat)
            obj.name = nDataset;
            obj.datasetMat = datasetMat;
            load(obj.datasetMat, 'X', 'Y');
            obj.X = obj.scaleFeatures(X);
            obj.Y = Y;
            obj.ratio = sum(obj.Y == -1) / sum(obj.Y == 1);
            [obj.d, obj.n] = size(obj.X);
        end
      
        function XScaled = scaleFeatures(~, X)
            XMax = repmat(max(X, [], 2), 1, size(X, 2));
            XMin = repmat(min(X, [], 2), 1, size(X, 2));
            
            XScaled = 2 * ((X - XMin) ./ (XMax - XMin)) - 1;
            
            XScaled(isnan(XScaled)) = X(isnan(XScaled));
        end

        function er = get.name(obj)
           er = obj.name;
        end
        function er = get.datasetMat(obj)
           er = obj.datasetMat;
        end
        function er = get.X(obj)
           er = obj.X;
        end
        function er = get.Y(obj)
           er = obj.Y;
        end
        function er = get.ratio(obj)
           er = obj.ratio;
        end
        function er = get.d(obj)
           er = obj.d;
        end
        function er = get.n(obj)
           er = obj.n;
        end
    end
end
